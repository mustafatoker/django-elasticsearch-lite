# -*- coding: utf-8 -*-
from optparse import make_option

from django.core.management.base import BaseCommand, CommandError
from django.db.models.loading import get_model

from elasticsearch_lite import bulk_index_async
from elasticsearch_lite.mixins import get_registered
from elasticsearch_lite.module_settings import get_setting


class Command(BaseCommand):
    help = "Index selected models "
    args = "<app_name> <model_name>"
    option_list = BaseCommand.option_list + (
        make_option('--filter',
                    action='store',
                    type='str',
                    dest='filter',
                    default=None,
                    help='comma-separated keyword arguments for filter method '
                    ' , eg. --where "is_active=True,type=XXX"'),
        make_option('--exclude',
                    action='store',
                    type='str',
                    dest='exclude',
                    default=None,
                    help='comma-separated keyword arguments for exlude method '
                    ' , eg. --where "is_active=True,type=XXX"'),
        make_option('--class',
                    action='store',
                    type='str',
                    dest='class',
                    default=None,
                    help='Use custom BulkIndexer class.'),
        make_option('--limit',
                    action='store',
                    type='int',
                    dest='limit',
                    default=None,
                    help='limit number of indexed objects'),
        make_option('--all',
                    action='store_true',
                    dest='all',
                    default=False,
                    help='Index all models with their default queryset'
                    '(conflicts with app/model arguments and filtering'
                    'options)'),
    )

    @staticmethod
    def _get_kwargs(self, kwargs_str):
        return dict(x.split('=') for x in ','.split(kwargs_str))

    def handle(self, *args, **options):
        # Get arguments
        if options['all'] and args:
            raise CommandError('--all options conflicts with model selection')
        elif options['all']:
            models = [m for m in get_registered()]
        elif len(args) < 1:
            raise CommandError('App and model not specified')
        elif len(args) < 2:
            models = [m._meta.object_name for m in get_registered(args[0])]
            raise CommandError(
                'Model not specified. Select one of: %s' % ', '.join(models)
            )
        else:
            app_label, model_name = args[:2]
            models = [get_model(app_label, model_name)]

        # Load custom BulkIndexer class
        bulk_cls = None
        cls_name = options['class'] or get_setting('ES_BULK_CLASS')
        if cls_name:
            try:
                klass_list = cls_name.split('.')
                klass_name = klass_list.pop()
                module_path = '.'.join(klass_list)
                module = __import__(module_path, globals(), locals(),
                                    [klass_name])
                bulk_cls = getattr(module, klass_name)
            except (ImportError, AttributeError, IndexError):
                raise CommandError('Could not import %s' % cls_name)

        # Index models
        len_models = len(models)
        for num, model in enumerate(models, 1):
            if len_models > 1:
                self.stdout.write("Indexing %s (%d of %d)" %
                                  (model._meta.object_name, num, len_models))
            self.index_single_model(model, bulk_cls, options)

    def index_single_model(self, model, bulk_cls, options):
        qset = model.objects.all().order_by('id')
        if not options['all']:
            self.apply_filters(qset, options)

        bulk_index_async(qset, klass=bulk_cls)

    def apply_filters(self, qset, options):
        if options['filter']:
            kwargs = self._get_kwargs(options['filter'])
            qset = qset.filter(kwargs)

        if options['exclude']:
            kwargs = self._get_kwargs(options['exclude'])
            qset = qset.exclude(kwargs)

        if options['limit']:
            qset = qset[:options('limit')]

        return qset
